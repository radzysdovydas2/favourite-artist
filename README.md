# Favourite-Artist web service

* provides an api to interact with your favourite artists.
* internally we integrate
  with [Itunes search api](https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/index.html#//apple_ref/doc/uid/TP40017632-CH3-SW1)

### Solution explanation

* Spring WebFlux - decided to use reactive api, since according to task description we expect to have large amount of
  users per month. Still need load testing to double-check whether this is a must. Since using spring webflux comes at a
  cost of code readability, testability and in-general makes it more complex.
* Spring Cache - in-memory caching to increase performance and hopefully, prevent users hitting itunes rate limits.
  Caffeine was chosen as cache provider, since it is well maintained and open-source tool, with features like expiration
  setup via application properties.
* To simplify service no in-memory persistence store like h2 was used, rather everything is in-memory.

### Known-Issues:

* Spring webflux APIs work best with non-blocking operations.
  In this particular case spring cache interactions are blocking operations. Have seen some suggestions to use Reactive
  redis for caching(Needs further research).

### Todo (not scope of homework exercise):

* [ ] Add Swagger specification
* [ ] Containerize application
* [ ] CI implementation
* [ ] Load test with either gatling, locust or jmeter
* [ ] Add support for other content type like video, audio etc..
