package lt.favouriteartist.rate.limit;

import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RateLimitService {

  private final AtomicInteger rateLimitCounter;

  public RateLimitService() {
    this.rateLimitCounter = new AtomicInteger(0);
  }

  public int getCount() {
    return this.rateLimitCounter.get();
  }

  public void increment() {
    this.rateLimitCounter.incrementAndGet();
  }

  public void clear() {
    this.rateLimitCounter.set(0);
  }
}
