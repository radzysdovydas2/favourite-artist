package lt.favouriteartist.rate.limit.scheduler;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.rate.limit.RateLimitService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RateLimitClearScheduler {

  private final RateLimitService rateLimitService;

  @Scheduled(cron = "0 0,59 * ? * * *") // at end of every hour
  public void clearRateLimit() {
    rateLimitService.clear();
  }
}
