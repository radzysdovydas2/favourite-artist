package lt.favouriteartist.integration.http.client;

import lt.favouriteartist.artist.dto.ArtistCollectionDto;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

public interface ItunesClient {

  <T> Mono<T> lookup(MultiValueMap<String, String> queryArgumentMap, final Class<T> clazz);

  Mono<ArtistCollectionDto> search(MultiValueMap<String, String> queryArgumentMap);
}
