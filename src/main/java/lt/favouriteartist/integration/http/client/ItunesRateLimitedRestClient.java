package lt.favouriteartist.integration.http.client;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.artist.dto.ArtistCollectionDto;
import lt.favouriteartist.rate.limit.RateLimitService;
import lt.favouriteartist.exception.TooManyRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ItunesRateLimitedRestClient implements ItunesClient {

  @Value("${favourite-artist.itunes.rate-limit:100}")
  private int limitRate;

  private final ItunesClient delegate;
  private final RateLimitService rateLimitService;

  @Override
  public <T> Mono<T> lookup(final MultiValueMap<String, String> queryArgumentMap, final Class<T> clazz) {
    Mono<T> rateLimitError = checkRateLimit();
    if (rateLimitError != null) {
      return rateLimitError;
    }
    return delegate.lookup(queryArgumentMap, clazz);
  }

  @Override
  public Mono<ArtistCollectionDto> search(final MultiValueMap<String, String> queryArgumentMap) {
    Mono<ArtistCollectionDto> rateLimitError = checkRateLimit();
    if (rateLimitError != null) {
      return rateLimitError;
    }
    return delegate.search(queryArgumentMap);
  }

  private <T> Mono<T> checkRateLimit() {
    if (rateLimitService.getCount() >= limitRate) {
      // add logging
      return Mono.error(TooManyRequestException::new);
    }
    rateLimitService.increment();
    return null;
  }
}
