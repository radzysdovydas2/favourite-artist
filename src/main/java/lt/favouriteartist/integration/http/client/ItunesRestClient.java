package lt.favouriteartist.integration.http.client;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.artist.dto.ArtistCollectionDto;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ItunesRestClient implements ItunesClient {

  private final WebClient webClient;

  @Override
  public <T> Mono<T> lookup(final MultiValueMap<String, String> queryArgumentMap, final Class<T> clazz) {
    return webClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/lookup")
            .queryParams(queryArgumentMap)
            .build())
        .retrieve()
        .bodyToMono(clazz);
  }

  @Override
  public Mono<ArtistCollectionDto> search(final MultiValueMap<String, String> queryArgumentMap) {
    return webClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/search")
            .queryParams(queryArgumentMap)
            .build())
        .retrieve()
        .bodyToMono(ArtistCollectionDto.class);
  }
}
