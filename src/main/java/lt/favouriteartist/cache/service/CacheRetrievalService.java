package lt.favouriteartist.cache.service;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CacheRetrievalService<T> {

  private final CacheManager cacheManager;

  public List<T> retrieveCollection(String key, String cacheName) {
    return Optional.ofNullable(cacheManager.getCache(cacheName))
        .map(cache -> (List<T>) cache.get(key, Collection.class))
        .orElseGet(List::of);
  }

  public Flux<T> retrieveReactiveFlux(String key, String cacheName) {
    return Optional.ofNullable(cacheManager.getCache(cacheName))
        .map(cache -> (Flux<T>) cache.get(key, Flux.class))
        .orElseGet(Flux::empty);
  }
}
