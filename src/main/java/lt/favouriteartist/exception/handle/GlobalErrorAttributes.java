package lt.favouriteartist.exception.handle;

import lt.favouriteartist.enums.ErrorAttributesKey;
import lt.favouriteartist.exception.TooManyRequestException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

  /**
   * WebClientResponseException wrapper mappings to HttpStatus
   */
  private final Map<Class<?>, HttpStatus> exceptionToHttpStatusMap = Map.of(
      WebClientResponseException.BadRequest.class, HttpStatus.BAD_REQUEST,
      WebClientResponseException.NotFound.class, HttpStatus.NOT_FOUND,
      WebClientResponseException.Unauthorized.class, HttpStatus.UNAUTHORIZED,
      WebClientResponseException.UnsupportedMediaType.class, HttpStatus.UNSUPPORTED_MEDIA_TYPE,
      WebClientResponseException.MethodNotAllowed.class, HttpStatus.METHOD_NOT_ALLOWED,
      WebClientResponseException.InternalServerError.class, HttpStatus.INTERNAL_SERVER_ERROR,
      TooManyRequestException.class, HttpStatus.TOO_MANY_REQUESTS
  );

  @Override
  public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
    Throwable error = getError(request);
    final String timestamp = LocalDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME);
    return exceptionToHttpStatusMap.entrySet().stream()
        .filter(entry -> entry.getKey() != null && entry.getKey().isInstance(error))
        .findFirst()
        .map(entry -> buildAttributeMap(entry.getValue(), error, timestamp))
        .orElseGet(() -> buildAttributeMap(error, timestamp));
  }

  private Map<String, Object> buildAttributeMap(final HttpStatus httpStatus, final Throwable error,
      final String timestamp) {
    return Map.of(
        ErrorAttributesKey.CODE.getKey(), httpStatus.value(),
        ErrorAttributesKey.MESSAGE.getKey(), Optional.ofNullable(error).map(Throwable::getMessage).orElse(""),
        ErrorAttributesKey.TIME.getKey(), timestamp
    );
  }

  private Map<String, Object> buildAttributeMap(final Throwable error, final String timestamp) {
    return Map.of(
        ErrorAttributesKey.CODE.getKey(), determineHttpStatus(error).value(),
        ErrorAttributesKey.MESSAGE.getKey(), error.getMessage(),
        ErrorAttributesKey.TIME.getKey(), timestamp
    );
  }

  private HttpStatus determineHttpStatus(Throwable error) {
    if (error instanceof ResponseStatusException) {
      ResponseStatusException err = (ResponseStatusException) error;
      return err.getStatus();
    }

    return MergedAnnotations.from(error.getClass(), MergedAnnotations.SearchStrategy.TYPE_HIERARCHY)
        .get(ResponseStatus.class)
        .getValue(ErrorAttributesKey.CODE.getKey(), HttpStatus.class)
        .orElse(HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
