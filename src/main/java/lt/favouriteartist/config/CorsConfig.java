package lt.favouriteartist.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {

  private static final String PATH_PATTERN = "/**";

  private static final String ALLOWED_METHODS = "*";

  @Value("${http.cors.origins.allowed:*}")
  private String[] allowedOrigins;

  @Value("${http.cors.maxAge:1800}")
  private Long corsMaxAge;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry
        .addMapping(PATH_PATTERN)
        .allowedMethods(ALLOWED_METHODS)
        .allowedOrigins(allowedOrigins)
        .maxAge(corsMaxAge);
  }

}
