package lt.favouriteartist.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

  @Value("${integration.itunes.base-url:https://itunes.apple.com}")
  private String itunesBaseUrl;

  @Bean
  public WebClient webClient() {
    return WebClient.builder()
        .baseUrl(itunesBaseUrl)
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .exchangeStrategies(exchangeStrategies())
        .build();
  }

  @Bean
  public Jackson2JsonDecoder jackson2JsonDecoder() {
    return new Jackson2JsonDecoder(jacksonObjectMapper(), MediaType.ALL);
  }

  @Bean
  public ObjectMapper jacksonObjectMapper() {
    return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  private ExchangeStrategies exchangeStrategies() {
    return ExchangeStrategies.builder()
        .codecs(clientDefaultCodecsConfigurer -> clientDefaultCodecsConfigurer.defaultCodecs()
            .jackson2JsonDecoder(jackson2JsonDecoder()))
        .build();
  }
}
