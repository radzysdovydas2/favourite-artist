package lt.favouriteartist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FavouriteArtistApplication {

  public static void main(String[] args) {
    SpringApplication.run(FavouriteArtistApplication.class, args);
  }

}
