package lt.favouriteartist.artist.api;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.artist.dto.ArtistDto;
import lt.favouriteartist.artist.dto.FavoriteArtistRequestDto;
import lt.favouriteartist.artist.service.ArtistService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/artist")
public class ArtistController {

  private final ArtistService artistService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Flux<ArtistDto> searchArtists(
      @RequestParam String entity,
      @RequestParam String term,
      @RequestParam String userId) {
    return Flux.fromIterable(artistService.searchArtists(entity, term, userId));
  }

  @GetMapping(path = "/favourite", produces = MediaType.APPLICATION_JSON_VALUE)
  public Flux<ArtistDto> getFavouriteArtistForUser(@RequestParam String userId) {
    return artistService.getFavouriteArtist(userId);
  }

  @PostMapping(path = "/favourite", produces = MediaType.APPLICATION_JSON_VALUE)
  public Flux<ArtistDto> saveArtist(@RequestBody FavoriteArtistRequestDto favoriteArtistRequestDto) {
    return artistService.saveFavouriteArtist(
        favoriteArtistRequestDto.getUserId(),
        favoriteArtistRequestDto.getAmgArtistIds()
    );
  }
}
