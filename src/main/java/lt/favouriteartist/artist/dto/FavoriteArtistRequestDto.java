package lt.favouriteartist.artist.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavoriteArtistRequestDto {

  @NotBlank
  private String userId;
  @Size(min = 1)
  private Set<@NotNull Long> amgArtistIds = new HashSet<>();
}
