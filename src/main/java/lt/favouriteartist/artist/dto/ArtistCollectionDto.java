package lt.favouriteartist.artist.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
public class ArtistCollectionDto {

  private int resultCount;
  @JsonProperty("results")
  private Collection<ArtistDto> artistDtoCollection;
}
