package lt.favouriteartist.artist.dto;

import lombok.Data;

@Data
public class ArtistDto {

  private String wrapperType;
  private String artistType;
  private String artistName;
  private String artistLinkUrl;
  private long artistId;
  private long amgArtistId;
  private String primaryGenreName;
  private int primaryGenreId;
}
