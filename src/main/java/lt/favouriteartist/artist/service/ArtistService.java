package lt.favouriteartist.artist.service;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.artist.dto.ArtistCollectionDto;
import lt.favouriteartist.artist.dto.ArtistDto;
import lt.favouriteartist.cache.service.CacheRetrievalService;
import lt.favouriteartist.integration.http.client.ItunesClient;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ArtistService {

  private final CacheRetrievalService<ArtistDto> cacheRetrievalService;
  private final ItunesClient itunesRateLimitedRestClient;

  @Cacheable(cacheNames = "artists", key = "#userId")
  public List<ArtistDto> searchArtists(String entity, String term, String userId) {
    final MultiValueMap<String, String> queryParamMap = new LinkedMultiValueMap<>();
    queryParamMap.add("entity", entity);
    queryParamMap.add("term", term);
    return itunesRateLimitedRestClient.search(queryParamMap)
        .flatMapIterable(ArtistCollectionDto::getArtistDtoCollection)
        .collectList()
        // TODO: Blocking here is expensive, need to re-thing different approach
        .block(Duration.ofSeconds(2));
  }

  @CachePut(cacheNames = "favourite-artists", key = "#userId")
  public Flux<ArtistDto> saveFavouriteArtist(final String userId, final Set<Long> amgArtistIds) {
    final List<ArtistDto> cachedArtists = cacheRetrievalService.retrieveCollection(userId, "artists");
    final List<ArtistDto> favouriteCachedArtists = cachedArtists.stream()
        .filter(artistDto -> amgArtistIds.contains(artistDto.getAmgArtistId()))
        .collect(Collectors.toList());
    if (favouriteCachedArtists.size() == amgArtistIds.size()) {
      return Flux.fromIterable(favouriteCachedArtists);
    }

    // if required artist for saving are not in cache, do another lookup
    final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
    queryParams.put("amgArtistId", amgArtistIds.stream().map(String::valueOf).collect(Collectors.toList()));
    return itunesRateLimitedRestClient.lookup(queryParams, ArtistCollectionDto.class)
        .flatMapIterable(ArtistCollectionDto::getArtistDtoCollection);
  }

  public Flux<ArtistDto> getFavouriteArtist(String userId) {
    return cacheRetrievalService.retrieveReactiveFlux(userId, "favourite-artists");
  }
}
