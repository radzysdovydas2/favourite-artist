package lt.favouriteartist.album.dto;

import lombok.Data;

import java.math.BigDecimal;

// TODO: this dto is too large, refactor into smaller pieces and reuse.
@Data
public class AlbumDto {

  private String wrapperType;
  private String collectionType;
  private String collectionName;
  private String collectionCensoredName;
  private String collectionViewUrl;
  private BigDecimal collectionPrice;
  private String collectionExplicitness;

  private long artistId;
  private long amgArtistId;
  private String artistName;
  private String artistViewUrl;

  private String artworkUrl60;
  private String artworkUrl100;

  private int trackCount;
  private String copyright;
  private String country;
  private String currency;

  private String releaseDate;
  private String primaryGenreName;
}
