package lt.favouriteartist.album.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
public class AlbumCollectionDto {

  private int resultCount;
  @JsonProperty("results")
  private Collection<AlbumDto> albumDtoCollection;
}
