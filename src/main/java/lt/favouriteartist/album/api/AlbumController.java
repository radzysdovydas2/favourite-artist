package lt.favouriteartist.album.api;

import lombok.RequiredArgsConstructor;
import lt.favouriteartist.album.dto.AlbumDto;
import lt.favouriteartist.album.service.AlbumService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Valid
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/album")
public class AlbumController {

  private final AlbumService albumService;

  @GetMapping(path = "/top", produces = MediaType.APPLICATION_JSON_VALUE)
  public Flux<AlbumDto> getTopAlbums(@NotBlank @RequestParam String userId, @RequestParam long amgArtistId) {
    return albumService.getTopAlbums(userId, amgArtistId);
  }
}
