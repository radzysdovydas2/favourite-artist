package lt.favouriteartist.album.service;

import lt.favouriteartist.album.dto.AlbumCollectionDto;
import lt.favouriteartist.album.dto.AlbumDto;
import lt.favouriteartist.integration.http.client.ItunesClient;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Flux;

@Service
public class AlbumService {

  private static final String DEFAULT_LIMIT = "5";
  private final ItunesClient itunesRateLimitedRestClient;
  private final MultiValueMap<String, String> queryParamMap;

  public AlbumService(final ItunesClient itunesRateLimitedRestClient) {
    this.itunesRateLimitedRestClient = itunesRateLimitedRestClient;
    this.queryParamMap = new LinkedMultiValueMap<>();
    this.queryParamMap.add("entity", "album");
    this.queryParamMap.add("limit", DEFAULT_LIMIT);

  }

  @Cacheable(cacheNames = "top-albums", key = "#userId")
  public Flux<AlbumDto> getTopAlbums(String userId, long amgArtistId) {
    queryParamMap.add("amgArtistId", String.valueOf(amgArtistId));
    return itunesRateLimitedRestClient.lookup(queryParamMap, AlbumCollectionDto.class)
        .flatMapIterable(AlbumCollectionDto::getAlbumDtoCollection)
        .filter(albumDto -> "album".equalsIgnoreCase(albumDto.getCollectionType()));
  }

}
