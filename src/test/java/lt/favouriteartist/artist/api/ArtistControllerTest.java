package lt.favouriteartist.artist.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.favouriteartist.artist.dto.ArtistDto;
import lt.favouriteartist.artist.dto.FavoriteArtistRequestDto;
import lt.favouriteartist.artist.service.ArtistService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(properties = "spring.main.web-application-type=reactive")
@AutoConfigureWebTestClient
class ArtistControllerTest {

  private static final String USER_ID = UUID.randomUUID().toString();

  @MockBean
  private ArtistService artistService;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void shouldSearchArtists() throws JsonProcessingException {
    String entity = "asdasd";
    String term = "abba";
    long amgArtistId = 22L;
    final ArtistDto artistDto = new ArtistDto();
    artistDto.setAmgArtistId(amgArtistId);
    artistDto.setWrapperType("artist");
    Mockito.when(artistService.searchArtists(entity, term, USER_ID)).thenReturn(List.of(artistDto));

    webTestClient.get()
        .uri(uriBuilder -> uriBuilder.path("/v1/artist")
            .queryParam("userId", USER_ID)
            .queryParam("entity", entity)
            .queryParam("term", term)
            .build())
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
        .expectBody().json(objectMapper.writeValueAsString(Set.of(artistDto)));
  }

  @ParameterizedTest
  @MethodSource("invalidQueryParameterMap")
  void shouldRespondWithBadRequest(String userId, String entity, String term) {
    Mockito.when(artistService.searchArtists(userId, entity, term)).thenReturn(List.of());

    webTestClient.get()
        .uri(uriBuilder -> uriBuilder.path("/v1/artist")
            .queryParam("userId", userId)
            .queryParam("entity", entity)
            .queryParam("term", term)
            .build())
        .exchange()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
        .expectStatus().isBadRequest();
  }

  @Test
  void shouldGetFavouriteArtists() throws JsonProcessingException {
    final ArtistDto artistDto = new ArtistDto();
    Mockito.when(artistService.getFavouriteArtist(USER_ID)).thenReturn(Flux.just(artistDto));

    webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path("/v1/artist/favourite")
            .queryParam("userId", USER_ID)
            .build())
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
        .expectBody().json(objectMapper.writeValueAsString(Set.of(artistDto)));
  }

  @Test
  void shouldSaveFavouriteArtist() throws JsonProcessingException {
    final long amgArtistId = 1L;
    final Set<Long> amgArtistIds = Set.of(amgArtistId);
    final ArtistDto artistDto = new ArtistDto();
    artistDto.setAmgArtistId(amgArtistId);
    final FavoriteArtistRequestDto favoriteArtistRequestDto = new FavoriteArtistRequestDto(USER_ID, amgArtistIds);
    Mockito.when(artistService.saveFavouriteArtist(USER_ID, amgArtistIds)).thenReturn(Flux.just(artistDto));

    webTestClient.post()
        .uri(uriBuilder -> uriBuilder.path("/v1/artist/favourite").build())
        .body(BodyInserters.fromValue(favoriteArtistRequestDto))
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
        .expectBody().json(objectMapper.writeValueAsString(Set.of(artistDto)));
  }

  private static Stream<Arguments> invalidQueryParameterMap() {
    return Stream.of(
        Arguments.of(null, null, null),
        Arguments.of("123", "allArtist", null),
        Arguments.of("123", null, "abba"),
        Arguments.of(null, "allArtist", "abba")
    );
  }
}
