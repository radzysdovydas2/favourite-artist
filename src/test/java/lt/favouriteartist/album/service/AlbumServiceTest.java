package lt.favouriteartist.album.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import lt.favouriteartist.album.dto.AlbumCollectionDto;
import lt.favouriteartist.album.dto.AlbumDto;
import lt.favouriteartist.integration.http.client.ItunesRestClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Set;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@Import({ AlbumServiceTest.Config.class, JacksonAutoConfiguration.class })
@ExtendWith(SpringExtension.class)
class AlbumServiceTest {

  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
  private WireMockServer server;
  @Autowired
  private AlbumService albumService;

  @Test
  void returnsProductsWhenSuccessful() throws JsonProcessingException {
    String userId = UUID.randomUUID().toString();
    long amgArtistId = 22L;
    final String queryParamString = String.format("entity=album&limit=5&amgArtistId=%s", amgArtistId);
    server.stubFor(
        get(urlMatching("/lookup\\?" + queryParamString))
            .willReturn(
                aResponse()
                    .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .withBody(objectMapper.writeValueAsString(buildAlbumCollectionDo()))));

    final Flux<AlbumDto> topAlbums = albumService.getTopAlbums(userId, amgArtistId);
    StepVerifier.create(topAlbums)
        .expectNext(buildAlbumDto())
        .verifyComplete();
  }

  private AlbumCollectionDto buildAlbumCollectionDo() {
    final AlbumCollectionDto albumCollectionDto = new AlbumCollectionDto();
    albumCollectionDto.setResultCount(1);
    albumCollectionDto.setAlbumDtoCollection(Set.of(buildAlbumDto()));
    return albumCollectionDto;
  }

  private AlbumDto buildAlbumDto() {
    final AlbumDto albumDto = new AlbumDto();
    albumDto.setAmgArtistId(22L);
    albumDto.setCollectionType("Album");
    return albumDto;
  }

  @TestConfiguration
  static class Config {
    @Bean
    public WireMockServer webServer() {
      WireMockServer wireMockServer = new WireMockServer(options().dynamicPort());
      wireMockServer.start();
      return wireMockServer;
    }

    @Bean
    public WebClient webClient(WireMockServer server) {
      return WebClient.builder().baseUrl(server.baseUrl()).build();
    }

    @Bean
    public AlbumService albumService() {
      return new AlbumService(new ItunesRestClient(webClient(webServer())));
    }
  }
}
