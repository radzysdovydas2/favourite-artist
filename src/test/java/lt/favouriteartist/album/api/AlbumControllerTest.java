package lt.favouriteartist.album.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.favouriteartist.album.dto.AlbumDto;
import lt.favouriteartist.album.service.AlbumService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.UUID;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest(properties = "spring.main.web-application-type=reactive")
@AutoConfigureWebTestClient
class AlbumControllerTest {

  @MockBean
  private AlbumService albumService;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void shouldGetTopAlbums() throws JsonProcessingException {
    String userId = UUID.randomUUID().toString();
    long amgArtistId = 22L;
    final AlbumDto albumDto = new AlbumDto();
    albumDto.setAmgArtistId(amgArtistId);
    Mockito.when(albumService.getTopAlbums(userId, amgArtistId)).thenReturn(Flux.just(albumDto));

    webTestClient.get()
        .uri(uriBuilder -> uriBuilder.path("/v1/album/top")
            .queryParam("userId", userId)
            .queryParam("amgArtistId", amgArtistId)
            .build())
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
        .expectBody().json(objectMapper.writeValueAsString(Set.of(albumDto)));
  }
}
