package lt.favouriteartist.cache.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CacheRetrievalServiceTest {

  private static final String ARTIST_CACHE_NAME = "artists";
  private static final String FAVOURITE_ARTIST_CACHE_NAME = "favourite-artist";
  private static final String USER_ID_KEY = "123";
  private static final String SOME_CACHE_VALUE = "some cache value";

  @Autowired
  private CacheManager cacheManager;
  @Autowired
  private CacheRetrievalService<String> cacheRetrievalService;

  @BeforeEach
  void setUp() {
    Optional.ofNullable(cacheManager.getCache(FAVOURITE_ARTIST_CACHE_NAME))
        .ifPresent(cache -> cache.put(USER_ID_KEY, Flux.just(SOME_CACHE_VALUE)));
    Optional.ofNullable(cacheManager.getCache(ARTIST_CACHE_NAME))
        .ifPresent(cache -> cache.put(USER_ID_KEY, List.of(SOME_CACHE_VALUE)));
  }

  @AfterEach
  void tearDown() {
    Stream.of(ARTIST_CACHE_NAME, FAVOURITE_ARTIST_CACHE_NAME)
        .forEach(cacheName -> Optional.ofNullable(cacheManager.getCache(cacheName)).ifPresent(Cache::clear));
  }

  @Test
  void shouldRetrieveCollection() {
    Assertions.assertFalse(cacheManager.getCacheNames().isEmpty());
    Assertions.assertTrue(cacheManager.getCacheNames().contains(ARTIST_CACHE_NAME));
    final List<String> cachedStringList = cacheRetrievalService.retrieveCollection(USER_ID_KEY, ARTIST_CACHE_NAME);
    Assertions.assertEquals(1, cachedStringList.size());
    Assertions.assertEquals(SOME_CACHE_VALUE, cachedStringList.get(0));
  }

  @Test
  void shouldRetrieveFlux() {
    Assertions.assertFalse(cacheManager.getCacheNames().isEmpty());
    Assertions.assertTrue(cacheManager.getCacheNames().contains(FAVOURITE_ARTIST_CACHE_NAME));
    StepVerifier.create(cacheRetrievalService.retrieveReactiveFlux(USER_ID_KEY, FAVOURITE_ARTIST_CACHE_NAME))
        .expectNext(SOME_CACHE_VALUE)
        .verifyComplete();
  }
}
